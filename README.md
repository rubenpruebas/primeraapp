**Se adjunta el proyecto comprimido en zip y el fichero de la actividad principal**
<br/>
<br/>
La aplicación avanza una estación cada vez que se pulsa el botón
<br/>
Cuando pasa de invierno a primavera se genera una cantidad aleatoria de años y se suma
<br/>
Aparece un mensaje indicando esa cantidad, el mensaje desaparece pasado un segundo y medio
<br/>
<br/>
_La aplicación cuenta con una versión en inglés y otra en español._
<br/>
<br/>

![ingles1](rdm/ingles1.png)
<br/>
**Estamos en invierno, con la siguiente estación sumaremos años**
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
![ingles2](rdm/ingles2.png)
<br/>
**Sumamos años, el mensaje desaparece en un segundo**
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
![ingles3](rdm/ingles3.png)
<br/>
**Ha desaparecido el mensaje**
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
![cambioidoma](rdm/cambioidoma.png)
<br/>
**Cambiamos el idioma para ver que funciona**
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
![espa](rdm/espa.png)
<br/>
**¡Funciona!  La carpeta con la version española debe llamarse /values-b+es/**

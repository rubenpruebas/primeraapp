package com.example.apli1


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.apli1.ui.theme.Apli1Theme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Apli1Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting()
                }
            }
        }
    }
}

// 0 inv   1 pri   2 ver   3 oto
var actual = 1
var annoActual = 0

@Composable
fun Greeting() {
    // esto no me dejaba asignarlo dentro de la funcion del click
    val invierno = stringResource(id = R.string.invierno)
    val primavera = stringResource(id = R.string.primavera)
    val verano = stringResource(id = R.string.verano)
    val otono = stringResource(id = R.string.otono)

    var ima by remember { mutableStateOf(R.drawable.pri) }
    var estacion by remember { mutableStateOf(primavera) }
    var colorEstacion by remember { mutableStateOf(Color.Green) }
    var mostrarAdding by remember { mutableStateOf(false) }
    var addAnnos by remember { mutableStateOf(0) }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Image(
            painter = painterResource(id = ima), contentDescription = "fotoEstacion",
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(15f / 19f)
                .scale(1f, 1.28f),
            contentScale = ContentScale.FillWidth
        )
        Surface(
            color = colorEstacion,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                Button(
                    modifier = Modifier
                        .padding(0.dp, 25.dp, 0.dp, 0.dp),
                    onClick = {
                        actual++
                        if (actual > 3) actual = 0

                        when (actual) {
                            0 -> {
                                ima = R.drawable.inv
                                estacion = invierno
                                colorEstacion = Color.Cyan
                            }

                            1 -> {
                                ima = R.drawable.pri
                                estacion = primavera
                                colorEstacion = Color.Green
                            }

                            2 -> {
                                ima = R.drawable.ver
                                estacion = verano
                                colorEstacion = Color.Yellow
                            }

                            3 -> {
                                ima = R.drawable.oto
                                estacion = otono
                                colorEstacion = Color.Gray
                            }
                        }
                        if (actual == 1) {
                            // Cambio de anno
                            addAnnos = (1..5).random()
                            CoroutineScope(Dispatchers.Default).launch {
                                mostrarAdding = true
                                annoActual += addAnnos
                                delay(1500)
                                mostrarAdding = false
                            }
                        }

                    }) {
                    Text(stringResource(id = R.string.siguienteEstacion))
                }
                Text(
                    text = stringResource(id = R.string.estacionActual) + estacion
                )
                Text(
                    text = stringResource(id = R.string.anno) + annoActual
                )
                if (mostrarAdding) {
                    Text(
                        text = "" + addAnnos + " " + stringResource(id = R.string.annadidos)
                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Apli1Theme {
        Greeting()
    }
}